#include <unistd.h>
#include <librpitx/librpitx.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <cmath>

bool running = true;

#define PROGRAM_VERSION "0.1"

void terminate(int num) {
    running = false;
    fprintf(stderr, "Caught signal - Terminating\n");
}

// Estrutura para armazenar dados do CSV
struct DopplerData {
    float dopplerShift;  // Deslocamento Doppler (em kHz)
    float noisePower;    // Nível de ruído (em dB)
};

std::vector<DopplerData> readCSV(const std::string &filename) {
    std::ifstream file(filename);
    std::vector<DopplerData> data;

    if (!file.is_open()) {
        throw std::runtime_error("Erro ao abrir o arquivo CSV.");
    }

    std::string line;
    std::getline(file, line); // Ignora o cabeçalho

    while (std::getline(file, line)) {
        std::istringstream ss(line);
        std::string time, dopplerShift, noisePower;

        std::getline(ss, time, ',');
        std::getline(ss, dopplerShift, ',');
        std::getline(ss, noisePower, ',');

        DopplerData entry;
        entry.dopplerShift = std::stof(dopplerShift); // Converte para float
        entry.noisePower = std::stof(noisePower);     // Converte para float
        data.push_back(entry);
    }

    file.close();
    return data;
}

int main(int argc, char *argv[]) {
    const float centerFrequency = 434e6; // Frequência central em Hz
    const int transmissionTimeUs = 100000; // Tempo de transmissão por linha (100 ms)

    int initialPowerLevel = 7; // Nível inicial de potência (0 a 7)

    // Lê o arquivo CSV
    std::string filename = "input.csv";
    std::vector<DopplerData> dopplerData;

    try {
        dopplerData = readCSV(filename);
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    // Configuração padrão do GPIO
    generalgpio gengpio;
    gengpio.setpulloff(4);

    padgpio pad;
    pad.setlevel(initialPowerLevel); // Define a potência inicial

    clkgpio *clk = new clkgpio;
    clk->SetAdvancedPllMode(true);

    clk->enableclk(4); // Ativa o clock no GPIO 4

    for (size_t i = 0; i < dopplerData.size() && running; ++i) {
        // Calcula a frequência com deslocamento Doppler
        float currentFrequency = centerFrequency + (dopplerData[i].dopplerShift * 1e3);

        // Ajusta a frequência central
        clk->SetCenterFrequency(currentFrequency, 10);

        // Ajusta o nível de potência baseado no ruído
        int currentPowerLevel = std::round(initialPowerLevel + dopplerData[i].noisePower / 10.0);
        if (currentPowerLevel > 7) currentPowerLevel = 7; // Limita ao máximo permitido
        if (currentPowerLevel < 0) currentPowerLevel = 0; // Limita ao mínimo permitido
        pad.setlevel(currentPowerLevel);

        // Imprime informações de depuração
        fprintf(stdout, "Linha %zu - Frequência: %.2f Hz, Potência: %d, Ruído: %.2f dB\n",
                i + 1, currentFrequency, currentPowerLevel, dopplerData[i].noisePower);

        // Aguarda o tempo de transmissão (100 ms)
        usleep(transmissionTimeUs);
    }

    clk->disableclk(4); // Desativa o clock no GPIO 4
    delete clk;

    return 0;
}
