#include <unistd.h>
#include <librpitx/librpitx.h>
#include "stdio.h"
#include <cstring>
#include <signal.h>
#include <stdlib.h>

bool running = true;

#define PROGRAM_VERSION "0.1"

void terminate(int num) {
    running = false;
    fprintf(stderr, "Caught signal - Terminating\n");
}

int main(int argc, char* argv[]) {
    float baseFrequency = 1e6; // Frequência inicial: 1 MHz
    float step = 20e3;         // Passo: 20 kHz
    int iterations = 100;      // Número de iterações
    int delayMicroseconds = 200000; // Intervalo de 200 milissegundos

    int initialPowerLevel = 7; // Nível inicial de potência (0 a 7)

    // Configuração padrão
    generalgpio gengpio;
    gengpio.setpulloff(4);

    padgpio pad;
    pad.setlevel(initialPowerLevel); // Define a potência inicial

    clkgpio *clk = new clkgpio;
    clk->SetAdvancedPllMode(true);

    clk->enableclk(4); // Ativa o clock no GPIO 4

    for (int i = 0; i < iterations && running; ++i) {
        // Calcula a frequência atual
        float currentFrequency = baseFrequency + i * step;

        // Ajusta a frequência
        clk->SetCenterFrequency(currentFrequency, 10);

        // Ajusta o nível de potência
        int currentPowerLevel = initialPowerLevel - (i / (iterations / initialPowerLevel));
        if (currentPowerLevel < 0) currentPowerLevel = 0; // Evita valores negativos
        pad.setlevel(currentPowerLevel);

        // Imprime informações de debug
        fprintf(stdout, "Iteração %d - Frequência: %.2f Hz, Potência: %d\n", i + 1, currentFrequency, currentPowerLevel);

        // Aguarda 200 milissegundos
        usleep(delayMicroseconds);
    }

    clk->disableclk(4); // Desativa o clock no GPIO 4
    delete clk;

    return 0;
}
