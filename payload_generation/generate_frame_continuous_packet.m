% Lecture du fichier CSV
data = readtable('input_data_shortest.csv'); % Assurez-vous que le fichier est dans le même répertoire
[num_rows, ~] = size(data);

% Paramètres communs
normalize = @(sig) sig/sqrt(sum(sum(abs(sig).^2))/numel(sig));
nexmonsamples = @(s,m) bitor(bitshift(bitand(int32(real(s) * m), hex2dec('ffff')), 16), ...
                    bitand(int32(imag(s) * m), hex2dec('ffff')));
ieeeenc = ieee_80211_encoder();
ieeeenc.set_rate(1);

mac_packet = [ ...
    '80000000ffffffffffff827bbef096e0827bbef096e0602f54e02a09000000006' ...
    '4001111000f4d79436f766572744368616e6e656c01088c129824b048606c0504' ...
    '0103000007344445202401172801172c01173001173401173801173c011740011' ...
    '764011e68011e6c011e70011e74011e84011e88011e8c011e0020010023021300' ...
    '30140100000fac040100000fac040100000fac0200002d1aef0917ffffff00000' ...
    '000000000000000000000000000000000003d16280f0400000000000000000000' ...
    '0000000000000000007f080000000000000040bf0cb259820feaff0000eaff000' ...
    '0c005012a000000c30402020202dd0700039301770208dd0e0017f20700010106' ...
    '80ea96f0be7bdd090010180200001c0000dd180050f2020101800003a4000027a' ...
    '4000042435e0062322f0046050200010000ce9405ef' ];
mac_data = reshape(de2bi(hex2dec(mac_packet'),4,'left-msb')',1,[]);

ltf_format = 'LTF';

% Début du fichier script
fileID = fopen('generate_frame_continuous_packet.sh','w');
fprintf(fileID, '#!/system/bin/sh\n\n');
disp('Début de la génération du script...');

for row = 1:num_rows
    f_shift = row * 1e6; % Décalage Doppler (kHz)
    fprintf('Traitement de la ligne %d/%d : Décalage Doppler = %.2f Hz\n', row, num_rows, f_shift);
    
    fs = ieeeenc.FS; % Fréquence d'échantillonnage

    [time_domain_signal_struct, encoded_bit_vector, symbols_tx_mat] = ...
        ieeeenc.create_standard_frame(mac_data, 0, 'LTF', [], [], []);
    tx_signal = time_domain_signal_struct.tx_signal;

    % Vecteur temporel pour le signal
    t = (0:length(tx_signal)-1) / fs; 

    % Application du décalage fréquentiel
    tx_signal_shifted = tx_signal .* exp(1j * 2 * pi * f_shift * t);

    % Normalisation du signal décalé
    tx_signal = normalize(tx_signal_shifted) * 10^(-11 / 20);

    % Plotting the FFT (Frequency Domain Analysis)
    N = length(tx_signal); % Number of FFT points
    f = (-N/2:N/2-1)*(fs/N); % Frequency axis
    tx_signal_fft = fftshift(fft(tx_signal)); % FFT and shift to center zero frequency

    % Normalizing time signal for transmission
    len = length(tx_signal);
    tx_signal = [tx_signal(:); zeros(ceil(len/1000)*1000 - len + 2000,1)];

    % Conversion au format Nexmon
    tx_signal_nexmon = nexmonsamples(tx_signal,10000);

    % Division en blocs
    l = 250;
    n = length(tx_signal_nexmon)/l;
    start_offset = 1500*4;
    tx_signal_nexmon_hdr = [ ...
        int32(start_offset + (0:n-1)*4*l); ...  % offsets
        repmat(int32(l * 4),1,n); ...           % longueur
        reshape(tx_signal_nexmon, l, n) ...     % données
    ];

    NEX_WRITE_TEMPLATE_RAM = 426;
    NEX_SDR_START_TRANSMISSION = 427;

    for i = 1:n
    fprintf(fileID, 'nexutil -s%d -b -l1500 -v%s\n', NEX_WRITE_TEMPLATE_RAM, ...
        matlab.net.base64encode(typecast(tx_signal_nexmon_hdr(:,i),'uint8')));
    end

    % Envoi de la commande de transmission après chaque paquet
    sdr_start_params = [ ...
        int32(length(tx_signal_nexmon)); ...    % nombre d'échantillons
        int32(start_offset/4); ...              % offset de départ
        int32(hex2dec('1001')); ...             % spécification du canal
        int32(40); ...                          % puissance
        int32(0); ...                           % mode sans fin
    ];

    fprintf(fileID, 'nexutil -s%d -b -l20 -v%s\n', NEX_SDR_START_TRANSMISSION, ...
        matlab.net.base64encode(typecast(sdr_start_params(:),'uint8')));

    for i = 1:n
        fprintf(fileID, 'nexutil -s%d -b -l1500 -v%s\n', NEX_WRITE_TEMPLATE_RAM, ...
            matlab.net.base64encode(typecast(tx_signal_nexmon_hdr(:,i),'uint8')));
    end

    % Envoi de la commande de transmission après chaque paquet
    sdr_start_params = [ ...
        int32(length(tx_signal_nexmon)); ...    % nombre d'échantillons
        int32(start_offset/4); ...              % offset de départ
        int32(hex2dec('e06a')); ...             % spécification du canal
        int32(40); ...                          % puissance
        int32(1); ...                           % mode sans fin
    ];

    fprintf(fileID, 'nexutil -s%d -b -l20 -v%s\n', NEX_SDR_START_TRANSMISSION, ...
        matlab.net.base64encode(typecast(sdr_start_params(:),'uint8')));

    % Ajout d'un délai de 0,5 seconde
    fprintf(fileID, 'sleep 0.5\n');
    
    fprintf('Ligne %d traitée et commande de transmission envoyée.\n', row);
end

fclose(fileID);
disp('Script généré avec succès : myframe.sh');
