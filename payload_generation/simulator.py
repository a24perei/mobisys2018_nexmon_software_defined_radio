import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import subprocess

def run_simulation():
    try:
        # Exécute le script Python
        subprocess.run(["python", "final_simulation.py"], check=True)
        messagebox.showinfo("Succès", "Simulation terminée avec succès !")
        open_gif()
    except subprocess.CalledProcessError as e:
        messagebox.showerror("Erreur", f"Erreur lors de l'exécution du script :\n{e}")
    except FileNotFoundError:
        messagebox.showerror("Erreur", "Le fichier 'final_simulation.py' est introuvable.")

def open_gif():
    gif_window = tk.Toplevel()
    gif_window.title("Mouvement du Satellite et du Drone")
    gif_window.geometry("800x600")

    gif_label = tk.Label(gif_window)
    gif_label.pack()

    gif_image = Image.open("satellite_drone_movement_fast.gif")
    gif_frames = [ImageTk.PhotoImage(gif_image.copy().convert("RGBA")) for _ in range(gif_image.n_frames)]
    gif_image.seek(0)

    def update_frame(frame_index):
        gif_label.config(image=gif_frames[frame_index])
        gif_window.after(50, update_frame, (frame_index + 1) % len(gif_frames))

    update_frame(0)

# Configuration de la fenêtre principale
def main():
    root = tk.Tk()
    root.title("Simulateur d'Effet Doppler")

    # Configurer la fenêtre
    root.geometry("400x300")

    # Ajouter un titre
    title_label = tk.Label(root, text="Simulateur d'Effet Doppler", font=("Helvetica", 16, "bold"))
    title_label.pack(pady=20)

    # Ajouter un bouton pour exécuter le script
    run_button = tk.Button(root, text="Exécuter la Simulation", command=run_simulation, font=("Helvetica", 12))
    run_button.pack(pady=20)

    # Afficher la fenêtre
    root.mainloop()

if __name__ == "__main__":
    main()
