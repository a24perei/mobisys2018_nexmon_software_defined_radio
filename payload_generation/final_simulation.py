from os import times
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import pandas as pd
from math import acos
import csv

# Load the CSV file
file_path = 'SatelliteInitial LLA Position.csv'
satellite_data = pd.read_csv(file_path)

# Datas
earth_radius = 6371                   # km
drone_cursing = 724/3.6               # Drone cursing speed (km/h)
drone_altitude = earth_radius + 9     # km
N=80000                               # Number of points for the drone (calculated to have one point/second)

# Extract time, latitude, longitude, and altitude data
latitudes = satellite_data['Lat (deg)'].values
longitudes = satellite_data['Lon (deg)'].values
altitudes = satellite_data['Alt (km)'].values
latitude_rates= satellite_data['Lat Rate (deg/sec)'].values
longitude_rates= satellite_data['Lon Rate (deg/sec)'].values
altitude_rates= satellite_data['Alt Rate (km/sec)'].values
time = [seconds for seconds in range (len(altitudes))]  #temps écoulé en secondes depuis le début de la simulation à chaque point

# Converting spherical → Cartesian coordinates
def latlonalt_to_cartesian(lat, lon, alt):
    lat_rad = np.radians(lat)
    lon_rad = np.radians(lon)
    r = earth_radius + alt
    x = r * np.cos(lat_rad) * np.cos(lon_rad)
    y = r * np.cos(lat_rad) * np.sin(lon_rad)
    z = r * np.sin(lat_rad)
    return x, y, z

# Converting spherical → cartesian velocity
def spherical_to_cartesian_velocity(r, theta, phi, theta_rate, phi_rate, altitude_rate):
    vx = (
        altitude_rate * np.cos(theta) * np.cos(phi)
        - r * np.sin(theta) * np.cos(phi) * theta_rate
        - r * np.cos(theta) * np.sin(phi) * phi_rate
    )
    vy = (
        altitude_rate * np.cos(theta) * np.sin(phi)
        - r * np.sin(theta) * np.sin(phi) * theta_rate
        + r * np.cos(theta) * np.cos(phi) * phi_rate
    )
    vz = (
        altitude_rate * np.sin(theta)
        + r * np.cos(theta) * theta_rate
    )
    return np.array([vx, vy, vz])

# Satelite trajectory (in cartesian coordinates)
satellite_x, satellite_y, satellite_z = latlonalt_to_cartesian(latitudes, longitudes, altitudes)
satellite_velocities=[]
for t in time :
  satellite_velocities.append(spherical_to_cartesian_velocity(altitudes[t], latitudes[t], longitudes[t], latitude_rates[t], longitude_rates[t], altitude_rates[t]))

# Liste des points du satellite :
Positions_satellite=[ [satellite_x[t], satellite_y[t], satellite_z[t]] for t in time]

# Drone trajectory

# Coordinate of Clermont-Ferrand (in km/radians)
C = [drone_altitude, 0.7718, 0.0538]   # [r, θ, φ]
# Coordinate of Adelaïde (in km/radians)
A = [drone_altitude, 2.180, 2.420]     # [r, θ, φ]

# Conversion in cartesian coordinates
C_cart = np.array([
    C[0] * np.sin(C[1]) * np.cos(C[2]),
    C[0] * np.sin(C[1]) * np.sin(C[2]),
    C[0] * np.cos(C[1])
])

A_cart = np.array([
    A[0] * np.sin(A[1]) * np.cos(A[2]),
    A[0] * np.sin(A[1]) * np.sin(A[2]),
    A[0] * np.cos(A[1])
])

# Vector normal to the plane (OAC)
n = np.array([
    A_cart[1] * C_cart[2] - A_cart[2] * C_cart[1],
    A_cart[2] * C_cart[0] - A_cart[0] * C_cart[2],
    A_cart[0] * C_cart[1] - A_cart[1] * C_cart[0]
])

# Normalisation of the vector n
n = n / np.linalg.norm(n)

# Definition of the vectors u and v : new frame
u = C_cart / np.linalg.norm(C_cart)  # Unitary vector in OC's direction
v = np.cross(n, u)
v = v / np.linalg.norm(v)            # Vector normal to u and n

# Stop angle
psca=np.dot(A_cart,C_cart)
alpha=acos(psca/(drone_altitude**2))

# Parametrisation of the great circle
t_values = np.linspace(0, -alpha, N)
Positions_drone = drone_altitude * (np.outer(np.cos(t_values), u) + np.outer(np.sin(t_values), v))
drone_x, drone_y, drone_z = Positions_drone[:, 0], Positions_drone[:, 1], Positions_drone[:, 2]


# Create the figure and the 3D axis
fig = plt.figure(figsize=(10, 7))
ax = fig.add_subplot(111, projection='3d')

# Create the figure and 3D axis
u, v = np.mgrid[0:2*np.pi:100j, 0:np.pi:50j]
earth_x = earth_radius * np.cos(u) * np.sin(v)
earth_y = earth_radius * np.sin(u) * np.sin(v)
earth_z = earth_radius * np.cos(v)
ax.plot_surface(earth_x, earth_y, earth_z, color='b', alpha=0.3, rstride=5, cstride=5, cmap=cm.Blues)

# Plot the satellite's trajectory
# ax.plot(satellite_x, satellite_y, satellite_z, color='red', label='Satellite Trajectory')

# Plot the drone's trajectory
# ax.plot(drone_x, drone_y, drone_z, color='green', label='Drone Trajectory')

# Define boundaries and labels
ax.set_xlim([-2 * earth_radius, 2 * earth_radius])
ax.set_ylim([-2 * earth_radius, 2 * earth_radius])
ax.set_zlim([-2 * earth_radius, 2 * earth_radius])
ax.set_xlabel("X (km)")
ax.set_ylabel("Y (km)")
ax.set_zlabel("Z (km)")
ax.set_title("Modélisation 3D des trajectoires du drone et du satellite")
ax.legend()

# plt.show()
print(len(Positions_drone))
print(len(altitudes))

import matplotlib.pyplot as plt

# Calculate the distance and relative speed between the satellite and the drone

# Calculating drone velocities
def position_a_vitesse (Pos, dt):
    # Numerical derivation of the position vector as a function of time
    l=len(Pos)
    V=[]     # list of velocity vectors
    for i in range (l-1):
        v=[]
        for j in range (3):
            v.append((Pos[i+1][j]-Pos[i][j])/dt)
        V.append(v)
    return V
drone_velocities=position_a_vitesse(Positions_drone,1)   # step of one second

# Calculating radial speed
def radial_velocity(X1, X2, V1, V2, dt):
    # Calculates the ratio-velocities at instants T from the velocities of the two modules
    V_radiale=[]
    l=min(len(X1), len(X2))
    for i in range(l-1):
        N=[X2[i][0]-X1[i][0], X2[i][1]-X1[i][1], X2[i][2]-X1[i][2]]
        norme=np.sqrt((N[0]**2)+(N[1]**2)+(N[2]**2))
        N=[N[0]/norme, N[1]/norme, N[2]/norme]

        v1_radiale=V1[i][0]*N[0]+V1[i][1]*N[1]+V1[i][2]*N[2]
        v2_radiale=V2[i][0]*N[0]+V2[i][1]*N[1]+V2[i][2]*N[2]

        v_radiale=v2_radiale-v1_radiale
        V_radiale.append(v_radiale)
    return V_radiale


l=min(len(Positions_drone), len(altitudes))
Radial_velocities=radial_velocity(Positions_drone, Positions_satellite, drone_velocities, satellite_velocities, 1)

#distances
distances=[np.linalg.norm(Positions_drone[i]-Positions_satellite[i]) for i in range(l)]



# Plot distance and relative speed

# Plot distance
plt.figure(figsize=(12, 6))
plt.plot(time[:l], distances, label="Distance (km)")
plt.xlabel("Time (seconds)")
plt.ylabel("Distance (km)")
plt.title("Distance Between Satellite and Drone Over Time")
plt.legend()
plt.grid(True)
# plt.show()

# Plot relative speed
plt.figure(figsize=(12, 6))
plt.plot(time[:l-1], Radial_velocities, label="Relative Velocity (km/s)", color="orange")
plt.xlabel("Time (seconds)")
plt.ylabel("Relative Velocity (km/s)")
plt.title("Relative Velocity Between Satellite and Drone Over Time")
plt.legend()
plt.grid(True)
# plt.show()

"""## Plotting GIF"""

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation, PillowWriter

# Configuração inicial do gráfico 3D
fig = plt.figure(figsize=(10, 7))
ax = fig.add_subplot(111, projection='3d')

# Configurações para os limites do gráfico e rótulos
ax.set_xlim([-2 * earth_radius, 2 * earth_radius])
ax.set_ylim([-2 * earth_radius, 2 * earth_radius])
ax.set_zlim([-2 * earth_radius, 2 * earth_radius])
ax.set_xlabel("X (km)")
ax.set_ylabel("Y (km)")
ax.set_zlabel("Z (km)")

# Criar a Terra como uma esfera
u, v = np.mgrid[0:2 * np.pi:100j, 0:np.pi:50j]
earth_x = earth_radius * np.cos(u) * np.sin(v)
earth_y = earth_radius * np.sin(u) * np.sin(v)
earth_z = earth_radius * np.cos(v)
ax.plot_surface(earth_x, earth_y, earth_z, color='b', alpha=0.3, rstride=5, cstride=5, cmap=cm.Blues)

# Configuração inicial para a animação mais rápida
skip_points = 100  # Quantidade de pontos a pular para acelerar o movimento

# Atualizar a função de animação para pular quadros
def update(frame):
    ax.cla()  # Limpar o gráfico para o próximo quadro

    # Redefinir os limites e o rótulo da Terra
    ax.plot_surface(earth_x, earth_y, earth_z, color='b', alpha=0.3, rstride=5, cstride=5, cmap=cm.Blues)
    ax.set_xlim([-2 * earth_radius, 2 * earth_radius])
    ax.set_ylim([-2 * earth_radius, 2 * earth_radius])
    ax.set_zlim([-2 * earth_radius, 2 * earth_radius])
    ax.set_xlabel("X (km)")
    ax.set_ylabel("Y (km)")
    ax.set_zlabel("Z (km)")

    # Calcular índice considerando o pulo de pontos
    index = frame * skip_points
    if index >= len(satellite_x):
        index = len(satellite_x) - 1  # Evitar ultrapassar o limite dos dados

    # Plotar a trajetória até o índice atual para ambos
    ax.plot(satellite_x[:index], satellite_y[:index], satellite_z[:index], color='red', label='Satellite Trajectory')
    ax.plot(drone_x[:index], drone_y[:index], drone_z[:index], color='green', label='Drone Trajectory')

    # Plotar o ponto atual do satélite e do drone
    ax.scatter(satellite_x[index], satellite_y[index], satellite_z[index], color='red', s=50)
    ax.scatter(drone_x[index], drone_y[index], drone_z[index], color='green', s=50)

    # Adicionar a legenda
    ax.legend()

# Criar a animação com os pontos pulados para aumentar a velocidade aparente
num_frames = len(satellite_x) // skip_points
ani = FuncAnimation(fig, update, frames=num_frames, interval=50)  # Interval para ajustar a suavidade

# Salvar como GIF
ani.save("satellite_drone_movement_fast.gif", writer=PillowWriter(fps=20))

"""## Doppler effect calculation and attenuation

![image.png](attachment:image.png)
"""

import numpy as np
import matplotlib.pyplot as plt

# Parameters
frequency = 13e9  # Transmission ferquency in Hz (13 GHz)
c = 3e8  # Speed of the light in m/s

# Lists for storing Doppler shift, signal level and distance between satellite and drone
doppler_shifts = []
signal_levels = []
print(len(distances))

# Calculate the Doppler shift, signal level and distance for each point
for i in range(l-1):
    # Current position of satellite and drone
    sat_pos = np.array([satellite_x[i], satellite_y[i], satellite_z[i]])
    drone_pos = np.array([drone_x[i], drone_y[i], drone_z[i]])

    # Calculate the Doppler shift
    doppler_shift = (Radial_velocities[i] / c) * frequency / 1000  # Em kHz
    doppler_shifts.append(doppler_shift)

    # Calculate the signal level in dB (Free-Space Path Loss)
    path_loss_dB = 20 * np.log10(4 * np.pi * distances[i] * frequency / c)
    signal_levels.append(-path_loss_dB)  # Using negative to represent the signal level in dB

# Plotar o efeito Doppler, o nível de sinal e a distância ao longo do tempo
plt.figure(figsize=(12, 12))

# Plot Doppler Shift
plt.subplot(3, 1, 1)
plt.plot(time[:l-1], doppler_shifts, label="Doppler Shift (kHz)", color="purple")
plt.xlabel("Time (seconds)")
plt.ylabel("Doppler Shift (kHz)")
plt.title("Doppler Shift Between Satellite and Drone Over Time")
plt.legend()
plt.grid(True)

# Plot Signal Level
plt.subplot(3, 1, 2)
plt.plot(time[:l-1], signal_levels, label="Signal Level (dB)", color="orange")
plt.xlabel("Time (seconds)")
plt.ylabel("Signal Level (dB)")
plt.title("Signal Level Between Satellite and Drone Over Time")
plt.legend()
plt.grid(True)

# Plot Distance
plt.subplot(3, 1, 3)
plt.plot(time[:l], distances, label="Distance (km)", color="blue")
plt.xlabel("Time (seconds)")
plt.ylabel("Distance (km)")
plt.title("Distance Between Satellite and Drone Over Time")
plt.legend()
plt.grid(True)

plt.tight_layout()
# plt.show()

# Export in csv
filename = 'trajectoire_dronnne.csv'
rows = zip(time[:l-1], drone_x, drone_y, drone_z, drone_velocities[0], drone_velocities[1], drone_velocities[2])
with open(filename, mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['Time (s)', 'pos x', 'pos y', 'pos z', 'vit x', 'vit y', 'vit z'])
    writer.writerows(rows)
print(f"Les données ont été exportées dans le fichier '{filename}'.")

# Export in csv
filename = 'modele1_doppler_level.csv'
rows = zip(time[:l-1], doppler_shifts, signal_levels)
with open(filename, mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['Time (s)', 'Doppler shift (kHz)', 'Attenuation (dB)'])
    writer.writerows(rows)
print(f"Les données ont été exportées dans le fichier '{filename}'.")

# Coordenadas iniciais e finais
initial_lat, initial_lon = 45.7772, 3.0870  # Clermont-Ferrand, França
final_lat, final_lon = -34.8796, 138.5943  # Adélaïde, Australia
initial_altitude = 0.3  # Altitude inicial do drone em km (300 m)

# Definindo o número de pontos para a trajetória do drone
num_points = len(satellite_x)  # Correspondendo ao número de pontos do satélite

# Função para interpolar latitude e longitude ao longo da trajetória
def interpolate_trajectory(start_lat, start_lon, end_lat, end_lon, num_steps):
    lats = np.linspace(start_lat, end_lat, num_steps)
    lons = np.linspace(start_lon, end_lon, num_steps)
    return lats, lons

# Gerar latitudes e longitudes interpoladas para o drone
drone_lats, drone_lons = interpolate_trajectory(initial_lat, initial_lon, final_lat, final_lon, num_points)

# Listas para armazenar a trajetória do drone em coordenadas cartesianas
drone_x, drone_y, drone_z = [], [], []

# Converter a trajetória do drone para coordenadas cartesianas e adicionar altitude
for lat, lon in zip(drone_lats, drone_lons):
    x, y, z = latlonalt_to_cartesian(lat, lon, initial_altitude)
    drone_x.append(x)
    drone_y.append(y)
    drone_z.append(z)

# Criar a figura e o eixo 3D para visualização
fig = plt.figure(figsize=(10, 7))
ax = fig.add_subplot(111, projection='3d')

# Plotar a Terra como uma esfera
u, v = np.mgrid[0:2*np.pi:100j, 0:np.pi:50j]
earth_x = earth_radius * np.cos(u) * np.sin(v)
earth_y = earth_radius * np.sin(u) * np.sin(v)
earth_z = earth_radius * np.cos(v)
ax.plot_surface(earth_x, earth_y, earth_z, color='b', alpha=0.3, rstride=5, cstride=5, cmap=cm.Blues)

# Plotar a trajetória do satélite
ax.plot(satellite_x, satellite_y, satellite_z, color='red', label='Satellite Trajectory')

# Plotar a trajetória do drone
ax.plot(drone_x, drone_y, drone_z, color='green', label='Drone Trajectory')

# Definir limites e rótulos
ax.set_xlim([-2 * earth_radius, 2 * earth_radius])
ax.set_ylim([-2 * earth_radius, 2 * earth_radius])
ax.set_zlim([-2 * earth_radius, 2 * earth_radius])
ax.set_xlabel("X (km)")
ax.set_ylabel("Y (km)")
ax.set_zlabel("Z (km)")
ax.set_title("3D Trajectory of Satellite and Drone from Clermont-Ferrand to Adélaïde")
ax.legend()

# plt.show()

import matlab.engine

print("Initialisation du moteur Matlab...")
eng = matlab.engine.start_matlab()

try:
    print("Exécution du script 'generatescript'...")
    eng.generate_frame_single_packet(nargout=0) 
    print("Script 'generatescript' exécuté avec succès.")

except Exception as e:
    print(f"Erreur lors de l'exécution du script 'generatescript':\n{e}")

finally:
    print("Arrêt du moteur Matlab...")
    eng.quit()
    print("Moteur Matlab arrêté avec succès.")

